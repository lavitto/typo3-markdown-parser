# TYPO3 Extension `Markdown Parser`

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg?style=for-the-badge)](https://paypal.me/pmlavitto)
[![Latest Stable Version](https://img.shields.io/packagist/v/lavitto/typo3-markdown-parser?style=for-the-badge)](https://packagist.org/packages/lavitto/typo3-markdown-parser)
[![TYPO3](https://img.shields.io/badge/TYPO3-markdown_parser-%23f49700?style=for-the-badge)](https://extensions.typo3.org/extension/markdown_parser/)
[![License](https://img.shields.io/packagist/l/lavitto/typo3-markdown-parser?style=for-the-badge)](https://packagist.org/packages/lavitto/typo3-markdown-parser)

> This extension adds a simple markdown content element to your TYPO3 website.

- **Demo**: [www.lavitto.ch/typo3-ext-markdown-parser](https://www.lavitto.ch/typo3-ext-markdown-parser)
- **Gitlab Repository**: [gitlab.com/lavitto/typo3-markdown-parser](https://gitlab.com/lavitto/typo3-markdown-parser)
- **TYPO3 Extension Repository**: [extensions.typo3.org/extension/markdown_parser](https://extensions.typo3.org/extension/markdown_parser/)
- **Found an issue?**: [gitlab.com/lavitto/typo3-markdown-parser/issues](https://gitlab.com/lavitto/typo3-markdown-parser/issues)

## 1. Introduction

### Features

- Simple and fast installation
- No configuration needed
- Works with internal and external markdown files
- Provides a Markdown-compatible t3editor to edit markdown directly in content element
- Based on Parsedown from http://parsedown.org

## 2. Installation

### Installation using Composer

The recommended way to install the extension is by using [Composer](https://getcomposer.org/). In your Composer based 
TYPO3 project root, just do `composer req lavitto/typo3-markdown-parser`.

### Installation from TYPO3 Extension Repository (TER)

Download and install the extension `markdown_parser` with the extension manager module.

## 3. Minimal setup

Not required.

## 4. Administration

### Create a markdown content

1) Create a new content element and select "Markdown Content"
2) Create or link markdown content
3) Save the content element

### Possible contents

- Link: Create a link to a local markdown-file or an external url
- Content: Write your markdown content directly in your content element with the t3editor (in markdown mode)

## 5. Configuration

Not required

## 6. Contribute

Please create an issue at https://gitlab.com/lavitto/typo3-markdown-parser/issues.

**Please use GitLab only for bug-reporting or feature-requests. For support use the TYPO3 community channels or contact us by email.**

## 7. Support

If you need private or personal support, contact us by email on [info@lavitto.ch](mailto:info@lavitto.ch). 

**Be aware that this support might not be free!**
