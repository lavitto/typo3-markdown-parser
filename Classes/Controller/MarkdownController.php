<?php
/**
 * This file is part of the "markdown_parser" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace Lavitto\MarkdownParser\Controller;

use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageRendererResolver;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class MarkdownController
 *
 * @package Lavitto\MarkdownParser\Controller
 */
class MarkdownController extends ActionController
{

    /**
     * Parsedown from http://parsedown.org
     * (c) Emanuil Rusev http://erusev.com
     *
     * @var \Parsedown
     */
    protected $parsedown;

    /**
     * Load Parsedown
     */
    public function initializeAction(): void
    {
        require_once ExtensionManagementUtility::extPath('markdown_parser') . 'Resources/Private/Php/Parsedown.php';
        $this->parsedown = new \Parsedown();
        $this->parsedown->setSafeMode(true);
    }

    /**
     * Returns the rendered html code of a markdown content
     *
     * @return string
     */
    public function indexAction(): string
    {
        $output = '';
        $markdownContent = $this->getMarkdownContent();
        if ($markdownContent) {
            try {
                $output = $this->parsedown->text($markdownContent);
            } catch (Exception $e) {
                $this->addFlashMessage(
                    $this->getLanguageService()->sL('LLL:EXT:markdown_parser/Resources/Private/Language/Plugin.xlf:parse_error.message'),
                    $this->getLanguageService()->sL('LLL:EXT:markdown_parser/Resources/Private/Language/Plugin.xlf:parse_error.title'),
                    AbstractMessage::ERROR
                );
                $output = DebuggerUtility::var_dump($e, 'Debug Information', 8, false, true, true);
            }
        }
        return $this->renderFlashMessages() . $output;
    }

    /**
     * Returns the markdown content from file or bodytext
     *
     * @return string
     */
    protected function getMarkdownContent(): string
    {
        $content = '';
        $data = $this->configurationManager->getContentObject()->data;
        if ($data['bodytext'] !== '') {
            $content = $data['bodytext'];
        } elseif ($data['pi_flexform'] !== '') {
            $content = $this->getFileContent($data['pi_flexform']);
        } else {
            $this->addFlashMessage(
                $this->getLanguageService()->sL('LLL:EXT:markdown_parser/Resources/Private/Language/Plugin.xlf:no_content.message'),
                $this->getLanguageService()->sL('LLL:EXT:markdown_parser/Resources/Private/Language/Plugin.xlf:no_content.title'),
                AbstractMessage::ERROR
            );
        }
        return $content;
    }

    /**
     * Gets the content of the markdown file
     *
     * @param string $fileLink
     * @return string
     */
    protected function getFileContent(string $fileLink): string
    {
        /** @var LinkService $linkService */
        $linkService = $this->objectManager->get(LinkService::class);
        $link = $linkService->resolve($fileLink);
        $content = '';
        if ($link['type'] === 'file') {
            /** @var File $file */
            $file = $link['file'];
            $content = $file->getContents();
        } elseif ($link['type'] === 'url') {
            $content = GeneralUtility::getUrl($link['url']);
        }
        return $content;
    }

    /**
     * Renders the Flash Messages
     *
     * @return string
     */
    protected function renderFlashMessages(): string
    {
        $output = '';
        $flashMessages = $this->getControllerContext()->getFlashMessageQueue()->getAllMessagesAndFlush();
        if ($flashMessages !== null && count($flashMessages) !== 0) {
            $output = GeneralUtility::makeInstance(FlashMessageRendererResolver::class)
                ->resolve()
                ->render($flashMessages);
        }
        return $output;
    }

    /**
     * Gets the language service
     *
     * @return LanguageService
     */
    protected function getLanguageService(): LanguageService
    {
        return $GLOBALS['LANG'];
    }
}
