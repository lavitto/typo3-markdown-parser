<?php
/**
 * This file is part of the "markdown_parser" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die('Access denied.');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:markdown_parser/Configuration/TSconfig/page.tsconfig">
');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Lavitto.MarkdownParser',
    'Markdown',
    [
        'Markdown' => 'index',
    ],
    [],
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);
