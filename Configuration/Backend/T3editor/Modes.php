<?php
/**
 * This file is part of the "markdown_parser" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

/**
 * Mode definitions for t3editor
 */
return [
    'markdown' => [
        'module' => 'cm/mode/markdown/markdown',
        'extensions' => ['markdown'],
    ]
];
