<?php
/**
 * This file is part of the "markdown_parser" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') or die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Lavitto.MarkdownParser',
    'Markdown',
    'LLL:EXT:markdown_parser/Resources/Private/Language/Tca.xlf:markdownparser_markdown.wizard.title',
    'ext-markdownparser-markdown'
);

if (version_compare(TYPO3\CMS\Core\Utility\VersionNumberUtility::getNumericTypo3Version(), '9.5', '>=') === true) {
    $t3editorFormat = 'markdown';
} else {
    $t3editorFormat = 'mixed';
}

$GLOBALS['TCA']['tt_content']['palettes']['markdownparser_markdown'] = [
    'label' => 'LLL:EXT:markdown_parser/Resources/Private/Language/Tca.xlf:markdownparser_markdown',
    'showitem' => 'pi_flexform;LLL:EXT:markdown_parser/Resources/Private/Language/Tca.xlf:tt_content.markdownparser_markdown.pi_flexform,
    --linebreak--, bodytext;LLL:EXT:markdown_parser/Resources/Private/Language/Tca.xlf:tt_content.markdownparser_markdown.bodytext'
];

$GLOBALS['TCA']['tt_content']['types']['markdownparser_markdown'] = [
    'showitem' => '
    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
        --palette--;;general,
        --palette--;;headers,
        --palette--;;markdownparser_markdown,
    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
        --palette--;;hidden,
        --palette--;;access,
    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
    --div--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category,categories,
    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription',
    'columnsOverrides' => [
        'pi_flexform' => [
            'config' => [
                'eval' => 'trim',
                'fieldControl' => [
                    'linkPopup' => [
                        'options' => [
                            'allowedExtensions' => 'markdown,mdown,mkdn,md,mkd,mdwn,mdtxt,mdtext,text,txt,Rmd',
                            'blindLinkFields' => 'class,params,target,title',
                            'blindLinkOptions' => 'folder,mail,page,spec'
                        ]
                    ]
                ],
                'max' => 1024,
                'ds' => null,
                'ds_pointerField' => null,
                'search' => null,
                'renderType' => 'inputLink',
                'size' => 50,
                'softref' => 'typolink',
                'type' => 'input'
            ],
            'l10n_display' => null
        ],
        'bodytext' => [
            'config' => [
                'format' => $t3editorFormat,
                'renderType' => 't3editor',
                'wrap' => 'off'
            ]
        ]
    ]
];
